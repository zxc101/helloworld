#include "UString.h"
#include <iostream>

UString::UString()
{
	cout << "Enter string: ";
	getline(cin, str);
	cout << endl;
}

void UString::PrintLine()
{
	cout << "Whole string: " << str << endl;
}

void UString::PrintCountLetters()
{
	cout << "Count letters: " << str.length() << endl;
}

void UString::PrintFristLetter()
{
	cout << "Frist letter: " << str[0] << endl;
}

void UString::PrintLastLetter()
{
	cout << "Last letter: " << str[str.size() - 1] << endl;
}
