#pragma once
#include <string>

using namespace std;

class UString
{
public:

	UString();
	void PrintLine();
	void PrintCountLetters();
	void PrintFristLetter();
	void PrintLastLetter();

private:

	string str;
};
