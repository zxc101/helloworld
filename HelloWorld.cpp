#include <iostream>
#include <string>

using namespace std;

struct Bag
{
	string books[];
};

struct Student
{
	int Age = 0;
	int Height = 0;
	string Name = "";
	Bag* myBag = nullptr;

	void GetInfo()
	{
		cout << "Student struct";
	}
};

int main()
{
	Student* ptr = new Student{ 10, 160, "Paul" };
	ptr->GetInfo();
}