#pragma once
int Squared(int v)
{
	return v * v;
}

int Sum(int a, int b)
{
	return a + b;
}

int SquaredSum(int a, int b)
{
	return Squared(Sum(a, b));
}